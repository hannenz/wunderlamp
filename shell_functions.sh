################################
#  WUNDERLAMP shell functions  #
################################
#
# @author Johannes Braun <johannes.braun@swu.de>
# @version 2024-02-07
# @package wunderlamp
#
# Convenience and pass-thru functions for frequently used commands
# You might want to source this file from your shell's rc file 
# (f.e. `~/.bashrc` or `~/.zshrc`)
#
# ```
# test -e ~/path/to/wunderlamp/shell_functions.sh && source /path/to/wunderlamp/shell_functions.sh
# ```
#
# !!! Important Note: 
# 		Keep this file in the wunderlamp directory!
#

WUNDERLAMP_COMPOSE_FILE=$(dirname $_)/docker-compose.yml

alias wunderlamp='docker compose --file=${WUNDERLAMP_COMPOSE_FILE}'

# Run any following command inside wunderlamp's php container
# Example:
# ```
# wunderlamp-cli php --version
# ```

# docker compose exec => Run command in existing / running container
# docker compose run  => Launch a new container and run command as one-off



##################
#  PHP Commands  #
##################

wunderlamp-cli() {
	docker compose --file=${WUNDERLAMP_COMPOSE_FILE} run --rm -it -v $PWD:/app -w /app -u $(id -u) ${WUNDERLAMP_PHP_VERSION:-php8} $*
}


# Composer 
composer() {
	docker compose --file=${WUNDERLAMP_COMPOSE_FILE} run --rm -it -v $PWD:/app -w /app -u $(id -u) ${WUNDERLAMP_PHP_VERSION:-php8} composer $*
}

# CakePHP CLI
cake() {
	docker compose --file=${WUNDERLAMP_COMPOSE_FILE} run --rm -it -v $PWD:/app -w /app -u $(id -u) ${WUNDERLAMP_PHP_VERSION:-php8} ./bin/cake $*
}



#######################
#  Database commands  #
#######################

# Pass-thru `mariadb` command
mariadb() {
	if [ -t 0 ] ; then # Check if stdin is a TTY 
		docker compose --file "${WUNDERLAMP_COMPOSE_FILE}" exec -it db mariadb $*
	else
		docker compose --file "${WUNDERLAMP_COMPOSE_FILE}" exec -i db mariadb $*
	fi
}

# Pass-thru `mariadb-dump` command
mariadb-dump() {
	docker compose --file "${WUNDERLAMP_COMPOSE_FILE}" exec -it db mariadb-dump $*
}

mycli() {
	docker compose --file=${WUNDERLAMP_COMPOSE_FILE} exec -it db mycli $*
}

alias mysql='mariadb'
alias mysqldump='mariadb-dump'

