# DockerLAMP

A containerized development environment with Apache, PHP, MariaDB, Xdebug.
Inspired by [this blog post](https://matthewsetter.com/docker-development-environment/), [this one](https://matthewsetter.com/setup-step-debugging-php-xdebug3-docker/), [this](https://lynn.zone/blog/xdebug-3-in-docker/) … and others ;-)



## Installation / Setup

1. Install `docker` and `docker-composer` for your platform

2. Clone this repository

3. Rename `example.env` to `.env` and adjust to your needs:

| Env. Var                            | Default value  | Description                                        |
| ---                                 | ---            | ---                                                |
| `DOCKERLAMP_PROJECT_DIR`            | $HOME/Projects | Path to the project's base directory               |
| `DOCKERLAMP_XDEBUG_MODE`            | develop,debug  | XDebug mode (add `profile` to enable profiling)    |
| `DOCKERLAMP_DATABASE_USERNAME`      | root           |                                                    |
| `DOCKERLAMP_DATABASE_ROOT_PASSWORD` |                |                                                    |
| `DOCKERLAMP_HTTP_PORT_PHP8`         | 4088           | The port to be used for PHP 8 over HTTP            |
| `DOCKERLAMP_HTTPS_PORT_PHP8`        | 4188           | The port to be used for PHP 8 over HTTPS           |
| `DOCKERLAMP_HTTP_PORT_PHP7`         | 4077           | The port to be used for PHP 7 over HTTP            |
| `DOCKERLAMP_HTTPS_PORT_PHP7`        | 4177           | The port to be used for PHP 7 over HTTPS           |
| `DOCKERLAMP_MAILPIT_SMTP_PORT`      | 1025           | Port for Mailpit to use for SMTP                   |
| `DOCKERLAMP_MAILPIT_HTTP_PORT`      | 8025           | Port for Mailpit's web interface                   |
| `DOCKERLAMP_ADMINER_PORT`           | 2210           | Port for Adminer Database management web interface |


Create a directory `Projects` in your home directory, this will be the
web servers `WEBROOT` where your projects will live. (You can
configure another directory as project directory by setting the
environment variable `DOCKERLAMP_PROJECT_DIR`  in `.env`)

If the directory does not exist, DockerLAMP will create it but it will
not be writable for your host's user, so you might need to do
something like `chown -R user:user ~/Projects` 

Same for `./var` and `./tmp`

Then run the docker containers:

```
docker-compose up --build -d
```




## Apache

Modules:

- rewrite
- vhost_alias
- ssl
- headers


Virtual Hosts are configured so that each project in the `WEBROOT` can be accessed by a subdomain of localhost, f.e.

```
https://project-foobar.localhost
```

will serve the project located at `${DOCKERLAMP_PROJECT_DIR}/project-foobar`



## PHP

Two PHP versions are available: 

- PHP 7.4 on Port 4077 (HTTP) / 4177 (HTTPS)

- PHP 8.1 on Port 4088 (HTTP) / 4188 (HTTPS)

Modules:

- pdo 
- pdo_mysql 
- mysqli 
- curl 
- simplexml 
- mbstring 
- intl 
- gd
- xdebug





## MySQL

You can access the MySQL console like this:


Note: On some environments it might be necessery to specify the host explicitly and use a numeric(!) ip address.



## Xdebug

This docker container uses Xdebug 3, be sure to use port 9003 in your editor/IDE!
You will also need to do some path mapping, here is the relevant working configuration vor vim's vdebug plugin:

```
let g:vdebug_options = {
\ 		"break_on_open" : 1,
\		"port" : 9003,
\		"path_maps" : {
\			"/var/www/html": "~/Sites"
\		}
\} 
```

To use profiling edit `.env` file in this directory and add 'profile'
to `XDEBUG_MODE` and restart docker container


Another pitfall: The docker container relies on `host.docker.internal`
for configuring the docker's host's ip address. However,
`host.docker.internal` is **not** available on Linux before docker
v20.10. It **should** work on linux though because of falling back to
auto discovery but I did not try yet.

On OSX and Windows this should just work fine (yet not tested either)


## Mailpit

[Mailpit](https://mailpit.axllent.org/) is a fake mail agent, which will be used to send emails. Access the "mailbox" from https://localhost:8025




## Host Shell Integration

You can source the `shell_functions.sh` file from your `~/.bashrc` /
`~/.zshrc` which will add (or override) some commands to your shell
that will be executed in DockerLAMP's PHP / MariaDB container:

- composer
- mysql
- mysqldump
- mariadb
- mariadb-dump
- bin/cake

